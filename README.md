# IP Emailer

When install on a server and combined with a cron job, it will email you the public IP address of the server, when it changes due to not having a static IP address. Can be used as a work around for a DDNS. currently configured to use gmail, but can be used with any service that supports TLS/SSL SMTP

Requirments:
smtplib
requests 
python 2.7
