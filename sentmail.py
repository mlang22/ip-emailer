import requests
import smtplib
import json

res = requests.get("https://api.myip.com")
ip = json.loads(res.content)
oldip = open("ip", "r")
hisip = oldip.read().replace('/n','')
if ip['ip'] == hisip:
    print("IP unchanged")
else:
    mail = smtplib.SMTP('smtp.gmail.com', 587)
    mail.starttls()
    mail.login("YOUR_IP", "YOUR_PASSWORD")
    message = ip['ip']
    mail.sendmail("YOUR_EMAIL", "YOUR_EMAIL", message)
    mail.quit()
    print(message)
    oldip = open("ip", "w+")
    oldip.write(ip['ip'])
    oldip.close()
